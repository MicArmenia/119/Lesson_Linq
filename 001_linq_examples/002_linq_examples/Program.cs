﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _002_linq_examples
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = CreateRandomEnumerable(50).ToArray();
            List<int> res11 = arr.Where(p => IsPrime(p)).ToList();
            List<int> res1 = arr.Where(IsPrime).ToList();

            if (arr.Any(IsPrime))
            {
                Console.WriteLine(arr.Max());
            }
            else
            {
                Console.WriteLine(arr.Min());
            }

            int[] arr1 = CreateRandomEnumerable(10, 10, 20).ToArray();
            int[] arr2 = CreateRandomEnumerable(10, 15, 25).ToArray();

            int[] arr3 = arr1.Distinct().ToArray();

            int[] arr4 = arr1.Distinct(arr2).ToArray();
        }

        static IEnumerable<int> CreateRandomEnumerable(int count, int minValue = 10, int maxValue = 100)
        {
            var rnd = new Random();
            for (int i = 0; i < count; i++)
                yield return rnd.Next(minValue, maxValue);
        }

        static bool IsPrime(int item)
        {
            if (item < 2)
                return false;

            if (item == 2)
                return true;

            if (item % 2 == 0)
                return false;

            var value = Math.Sqrt(item);
            for (int a = 3; a <= value; a += 2)
            {
                if (item % a == 0)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
