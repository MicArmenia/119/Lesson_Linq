﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _002_linq_examples
{
    static class EnumerableExtensions
    {
        public static IEnumerable<int> Distinct(this IEnumerable<int> source, IEnumerable<int> source1)
        {
            var set = new HashSet<int>();
            foreach (int item in source)
                set.Add(item);

            foreach (int item in source1)
                set.Add(item);

            return set;
        }
    }
}
