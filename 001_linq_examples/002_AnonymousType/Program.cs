﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _002_AnonymousType
{
    class Program
    {
        static void Main(string[] args)
        {
            var student = new
            {
                Name = "Karen",
                Age = 36
            };

            //student.Name = "";

            Type t1 = student.GetType();
        }
    }
}
