﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _001_linq_examples
{
    class Student
    {
        public string name;
        public string surname;

        public string Fullname => $"{surname} {name}";

        public byte mark;
        public int age;

        public University university;

        public override string ToString()
        {
            return university.ToString();
        }
    }

    public enum University
    {
        Politexnik,
        PetHamalsaran,
        Mankavarshakan
    }
}