﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _001_linq_examples
{
    class Program
    {
        static bool Any(IEnumerable<Student> source)
        {
            foreach (var item in source)
                return true;
            return false;
        }

        static IEnumerable<string> SelectFullName(IEnumerable<Student> source)
        {
            foreach (var item in source)
            {
                if (item.university == University.Politexnik)
                    yield return item.Fullname;
            }
        }

        static void Main(string[] args)
        {
            List<Student> students = CreateStudents(50).ToList();
            if(!students.IsNullOrEmpty())
            {

            }

            List<Student> res1 = students
                .Where(p => p.university == University.Politexnik)
                .ToList();

            List<string> res2 = students
                .Select(p => p.Fullname)
                .ToList();


            var res22 = students
                .Select(p => new { p.Fullname, Mark = p.mark })
                .ToList();

            List<string> res3 = students
                .Where(p => p.university == University.Mankavarshakan)
                .Select(p => p.Fullname)
                .ToList();

            var res4 = students
                .Where(p => p.university == University.Politexnik)
                .Select(p => new
                {
                    p.name,
                    university = p.university
                })
                .ToList();

            Student st1 = students
                .FirstOrDefault(p => p.university == University.Politexnik);
            
            if(students.Any(p => p.university == University.Politexnik))
            {

            }
            
            double average = students
                .Where(p => p.university == University.Politexnik)
                .Average(p => p.mark);

            List<Student> res5 = students.Take(5).ToList();
            List<Student> res6 = students.Skip(5).Take(5).ToList();

            List<Student> res7 = students.OrderBy(p => p.mark).ToList();
            List<Student> res8 = students
                .OrderBy(p => p.university)
                .ThenBy(p => p.name)
                .ToList();


            Dictionary<University, List<Student>> resDic1 = students
                .GroupBy(p => p.university)
                .ToDictionary(p => p.Key, p => p.ToList());

            Dictionary<byte, List<Student>> resDic2 = students
                .GroupBy(p => p.mark)
                .ToDictionary(p => p.Key, p => p
                    .Where(p1 => p1.mark > 7)
                    .OrderByDescending(p1 => p1.mark)
                    .Take(10)
                    .ToList());

            Dictionary<University, List<Student>> resDic3 = students
                .GroupBy(p => p.university)
                .ToDictionary(p => p.Key, p => p
                    .Where(p1 => p1.mark > 7)
                    .OrderByDescending(p1 => p1.mark)
                    .ToList());
        }

        static IEnumerable<Student> CreateStudents(int count)
        {
            var rnd = new Random();
            for (int i = 0; i < count; i++)
            {
                yield return new Student
                {
                    name = $"A{(i + 1)}",
                    surname = $"A{(i + 1)}yan",
                    mark = (byte)rnd.Next(1, 21),
                    age = rnd.Next(15, 35),
                    university = (University)rnd.Next(0, 3)
                };
            }
        }
    }
}
